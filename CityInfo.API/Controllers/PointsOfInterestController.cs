﻿// Christian Schou
// PointsOfInterestController.cs
//
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using CityInfo.API.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using CityInfo.API.Services;
namespace CityInfo.API.Controllers
{
    [Route("api/cities")]
    public class PointsOfInterestController : Controller
    {
        private ILogger<PointsOfInterestController> _logger;
        private IMailService _mailService;
        private ICityInfoRepository _cityInfoRepository;

        // Contructor injection with logging
        public PointsOfInterestController(ILogger<PointsOfInterestController> logger, 
            IMailService mailService,
            ICityInfoRepository cityInfoRepository)
        {
            _logger = logger;
            _mailService = mailService;
            _cityInfoRepository = cityInfoRepository;
        }

        /// <summary>
        /// Gets a list of points of interest for a specific city
        /// </summary>
        /// <param name="cityId">City Id</param>
        /// <returns>Returns a list of points of interest for a specific city</returns>
        [HttpGet("{cityId}/pointsofinterest")]
        public IActionResult GetPointsOfInterest(int cityId)
        {
            try
            {
                // If city is not found return a not found status code
                if (!_cityInfoRepository.CityExists(cityId))
                {
                    _logger.LogInformation($"City with id {cityId} wasn't found when accessing points of interest.");
                    return NotFound();
                }

                var pointsOfInterestForCity = _cityInfoRepository.GetPointsOfInterestForCity(cityId);

                var pointsOfInterestForCityResults =
                    Mapper.Map<IEnumerable<PointOfInterestDto>>(pointsOfInterestForCity);

                // Then we return this list of DTOs
                return Ok(pointsOfInterestForCityResults);
            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting points of interest for city ith id {cityId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        /// <summary>
        /// Get point of specific point of interest for a specific city
        /// </summary>
        /// <param name="cityId">City Id</param>
        /// <param name="id">Point of Interest Id</param>
        /// <returns>Returns a specific point of interest</returns>
        [HttpGet("{cityId}/pointsofinterest/{id}", Name = "GetPointOfInterest")]
        public IActionResult GetPointOfInterest(int cityId, int id)
        {
            // First we will check if the city exists
            if (!_cityInfoRepository.CityExists(cityId))
            {
                return NotFound();
            }

            // Lets try get the point of interest for the city
            // if we get null back , we return 404 not found HTTP status
            var pointOfInterest = _cityInfoRepository.GetPointOfInterestForCity(cityId, id);

            if (pointOfInterest == null)
            {
                return NotFound();
            }

            // Otherwise we map the POI to a DTO
            var pointOfInterestResult = Mapper.Map<PointOfInterestDto>(pointOfInterest);

            // Then we return that DTO
            return Ok(pointOfInterestResult);

        }

        /// <summary>
        /// This let's you create a point of interest in the database
        /// </summary>
        /// <param name="cityId">City Id</param>
        /// <param name="pointOfInterest">Data from body</param>
        /// <returns>Returns a response with the location header for the created Point of interest</returns>
        [HttpPost("{cityId}/pointsofinterest")]
        public IActionResult CreatePointOfInterest(int cityId,
            [FromBody] PointOfInterestForCreationDto pointOfInterest)
        {
            if (pointOfInterest == null)
            {
                return BadRequest();
            }

            if (pointOfInterest.Description == pointOfInterest.Name)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_cityInfoRepository.CityExists(cityId))
            {
                return NotFound();
            }

            
            var finalPointOfInterest = Mapper.Map<Entities.PointOfInterest>(pointOfInterest);

            _cityInfoRepository.AddPointOfInterestForCity(cityId, finalPointOfInterest);

            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happend while handling your request.");
            }

            var createdPointOfInterestToReturn = Mapper.Map<Models.PointOfInterestDto>(finalPointOfInterest);

            // Lets us return a response with the location header
            // since the route template needs city id and the id of the
            // point of interest, we will pass in an anonamus type, with those values
            return CreatedAtRoute("GetPointOfInterest", new
            {cityId, id = createdPointOfInterestToReturn.Id}, createdPointOfInterestToReturn);
        }

        /// <summary>
        /// Update a specific point of interest
        /// </summary>
        /// <param name="cityId">City Id</param>
        /// <param name="id">Point of Interest Id</param>
        /// <param name="pointOfInterest">Data from body</param>
        /// <returns>Returns 204 No Content status if POI was updated</returns>
        [HttpPut("{cityId}/pointsofinterest/{id}")]
        public IActionResult UpdatePointOfInterest(int cityId, int id, 
                                                   [FromBody] PointOfInterestForUpdateDto pointOfInterest)
        {
            // Check if the POI exists, else return a Bad Request status
            if (pointOfInterest == null)
            {
                return BadRequest();
            }

            // Check for lazy people
            // Just tells the user that they cannot copye the title into the description
            if (pointOfInterest.Description == pointOfInterest.Name)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            // If the required data is not there, give them a bad request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // If the city doens't exist return a 404 Not Found status
            if (!_cityInfoRepository.CityExists(cityId))
            {
                return NotFound();
            }

            // Check if the point of interest exists on the city
            var pointOfInterestEntity = _cityInfoRepository.GetPointOfInterestForCity(cityId, id);
            if (pointOfInterestEntity == null)
            {
                return NotFound();
            }

            // Map the POIDto to the Entity
            Mapper.Map(pointOfInterest, pointOfInterestEntity);

            // If we were not able to save the data, return a server error
            // with the following message
            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            // Return a 204 No content HTTP status code
            // means that the request completed succesfully, but we got nothing to return
            return NoContent();
        }

        /// <summary>
        /// Allows you to partially update a point of interest using PATCH
        /// </summary>
        /// <param name="cityId">City Id</param>
        /// <param name="id">Point of Interest Id</param>
        /// <param name="patchDocument">Data from body</param>
        /// <returns>Return 204 No Content if the POI was updated</returns>
        [HttpPatch("{cityId}/pointsofinterest/{id}")]
        public IActionResult PartiallyUpdatePointOfInterest(int cityId, int id, 
                                                            [FromBody] JsonPatchDocument<PointOfInterestForUpdateDto> patchDocument)
        {
            //Check if data was entered
            if (patchDocument == null)
            {
                return BadRequest();
            }

            // If the city does not exist return 404 Not Found
            if (!_cityInfoRepository.CityExists(cityId))
            {
                return NotFound();
            }

            // If the point of interest does not exist return a 404 Not Found
            var pointOfInterestEntity = _cityInfoRepository.GetPointOfInterestForCity(cityId, id);
            if (pointOfInterestEntity == null)
            {
                return NotFound();
            }

            // Map the DTO to the Entity using AutoMapper in a new variable
            var pointOfInterestToPatch = Mapper.Map<PointOfInterestForUpdateDto>(pointOfInterestEntity);

            // Apply the new data to the object
            patchDocument.ApplyTo(pointOfInterestToPatch, ModelState);

            // Check if the required data is there
            if (!ModelState.IsValid)
            {
                // else return a Bad Request status
                return BadRequest(ModelState);
            }

            // Little validation check
            // We dont want lazy people to just copy the title into the description
            // Give the user an error if they do that
            if (pointOfInterestToPatch.Description == pointOfInterestToPatch.Name)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            // If we got this far, lets validate the POI we want to patch
            TryValidateModel(pointOfInterestToPatch);

            // If it aint valid - return a Bad Request status
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Passing in the source object which is the patched DTO 
            // and the destination object which is the entity
            Mapper.Map(pointOfInterestToPatch, pointOfInterestEntity);

            // If we were not able to save the data, return a server error
            // with the following message
            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            // Return a 204 No content HTTP status code
            // means that the request completed succesfully, but we got nothing to return
            return NoContent();
        }

        /// <summary>
        /// Allows you to delete a specific POI in the database
        /// </summary>
        /// <param name="cityId">City Id</param>
        /// <param name="id">Point of Interest Id</param>
        /// <returns>Returns 204 No Content if the POI was deleted from the database</returns>
        [HttpDelete("{cityId}/pointsofinterest/{id}")]
        public IActionResult DeletePointOfInterest(int cityId, int id)
        {
            // Check if the city exists
            if (!_cityInfoRepository.CityExists(cityId))
            {
                return NotFound();
            }

            // Check if the point of interest exists in our Database using the repository pattern
            var pointOfInterestEntity = _cityInfoRepository.GetPointOfInterestForCity(cityId, id);
            if (pointOfInterestEntity == null)
            {
                // If the POI doesn't exist return 404 Not Found
                return NotFound();
            }

            // Remove the point of interest
            _cityInfoRepository.DeletePointOfInterest(pointOfInterestEntity);

            // Call save to effectively remove the context from the database
            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            // Send a notification mail
            _mailService.Send("Point of interest deleted.", 
                              $"Point of interest{pointOfInterestEntity.Name}with id {pointOfInterestEntity.Id} was deleted.");

            // Inform the user that the operation succeeded with a 204 No Content status
            return NoContent();
        }
    }
}
